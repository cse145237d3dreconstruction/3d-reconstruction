# README #

Check out our video demo [here](https://www.youtube.com/watch?v=jk5aJdBHC44&feature=youtu.be)!

### What is this repository for? ###

* This project aims to provide archaeologists with a mobile system that can reconstruct an underground environment, with real-time partial or full feedback.
* [Wiki Page](https://bitbucket.org/cse145237d3dreconstruction/3d-reconstruction/wiki/Home)
* [Source code for Tango](https://github.com/ssl024/TangoMapper)
* [Source code for Kinect](https://bitbucket.org/cse145237d3dreconstruction/3d-reconstruction/src/2041ced88c51465708d8e32cb05b581b57dfba23/?at=kinect)

### How do I get set up? ###
* [Set up for Tango using Unity](https://bitbucket.org/cse145237d3dreconstruction/3d-reconstruction/wiki/Set%20Up%20Instructions%20(Project%20Tango%20Unity)) 
* [Setup for Kinect](https://bitbucket.org/cse145237d3dreconstruction/3d-reconstruction/wiki/Set%20up%20for%20Kinect)

### Who do I talk to? ###

* [Contact Info](https://bitbucket.org/cse145237d3dreconstruction/3d-reconstruction/wiki/Team%20Members)